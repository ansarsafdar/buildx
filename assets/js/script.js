// $(document).ready(function(){
//     $(".openBtn").click(function(){
//         $(".dropdown").addClass("active")
//     })
// })
$(function(){
    $(".openBtn").click(function(){
        $(".dropdown").toggleClass('active')
    })
    $(".menu-closeBtn").click(function(){
        $(".dropdown").toggleClass('active')
    })
    $(".playIcon").on('click', function(){
        $(this).hide();
        $(this).parent().find(".poster").hide();
        $(this).parent().find(".pauseIcon").show();
        let video = $(this).parent().find("video")[0]; 
        video.play();
    });
    
    
    $(".pauseIcon").on('click', function(){
        $(this).hide();
        $(this).parent().find(".poster").show();
        $(this).parent().find(".playIcon").show();
        let video = $(this).parent().find("video")[0]; 
        video.pause();
    });
    $(".tabs li").click(function () {  
        $(".tabs li").removeClass('active') 
        $(this).addClass('active')
        let selected_category = $(this).attr("id");
        if (selected_category == "all") {
          $("[data-category]").show();
        } else {
          $("[data-category]").hide();
          $('[data-category="' + selected_category + '"]').show();
        }
      });    
    
})